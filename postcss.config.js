module.exports = {
  plugins: {
    'postcss-pxtorem': {
      // rootValue: 37.5,
      rootValue ({ file }) {
        return file.indexOf('vant') !== -1 ? 37.5 : 75
      },
      // 配置要转换的 CSS 属性
      propList: ['*'],
      // 配置不做单位转换的资源文件名
      exclude: 'github-markdown'
    }
  }
}
