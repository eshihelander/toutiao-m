import Vue from 'vue'
import Vuex from 'vuex'

import { setItem, getItem } from '@/utils/storage'

Vue.use(Vuex)

const TOKEN_KEY = 'TOUTIAO_USER_TOKEN'

export default new Vuex.Store({
  state: {
    // user: null // 这个user会存储一个含有token字符串的对象
    // user: JSON.parse(window.localStorage.getItem('TOKEN_KEY'))
    user: getItem(TOKEN_KEY)
  },
  // mutation 变化的意思
  // 在mutation中定义add方法，修改count
  mutations: {
    setUser (state, data) {
      // 把数据临时存储到项目容器store中
      state.user = data
      // window.localStorage.setItem('TOKEN_KEY', JSON.stringify(state.user))
      // 把数据持久存储到本地 避免刷新后数据丢失
      setItem(TOKEN_KEY, state.user)
    }
  },
  actions: {},
  modules: {}
})
