import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/utils/dayjs'

// 加载Vant核心组件库
import Vant from 'vant'
// 行不行 try try see

// 加载 Vant 全局样式 后缀.css不能省略
import 'vant/lib/index.css'

// 加载全局样式 后缀 .less不能省略
// 注意我们自己的样式要放在Vant样式下面 否则会导致自己的样式被Vant样式覆盖
import '@/styles/index.less'

// 加载 rem 动态设置
import 'amfe-flexible'

// 注册使用 Vant 组件库
Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
