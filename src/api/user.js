/**
 * 用户请求相关模块
 */

import request from '@/utils/request'
// import store from '@/store'

export const login = (data) => {
  return request({
    method: 'POST',
    url: '/v1_0/authorizations',
    data
  })
}

/**
 * 发送验证码请求
 * 注意每个手机号接收到的验证码有效期为一分钟
 */
export const sendSms = (mobile) => {
  return request({
    method: 'GET',
    url: `/v1_0/sms/codes/${mobile}`
    // url: '/v1_0/sms/codes/:' + mobile
  })
}

/**
 * 获取用户个人信息
 *
 * 因为这个文件不是.vue组件 而是.js文件 所以不能直接使用 $store.state.user.token 来调用容器中的数据
 * 在.js文件中使用容器中的数据  需要先导入进来 import store from '@/store'
 */
export const getUserInfo = () => request({
  method: 'GET',
  url: '/v1_0/user'
  // headers: {
  //   Authorization: 'Bearer ' + store.state.user.token
  // }
})

/**
 * 用户关注、点赞、收藏模块
 */

// 关注请求
export const addFollow = target => request({
  method: 'POST',
  url: '/v1_0/user/followings',
  data: {
    target
  }
})

// 取消关注请求
export const cancelFollow = target => request({
  method: 'DELETE',
  url: `/v1_0/user/followings/${target}`
})

// 收藏请求
export const addCollect = target => request({
  method: 'POST',
  url: '/v1_0/article/collections',
  data: {
    target
  }
})

// 取消收藏请求
export const cancelCollect = target => request({
  method: 'DELETE',
  url: `/v1_0/article/collections/${target}`
})

// 点赞请求
export const addLike = target => request({
  method: 'POST',
  url: '/v1_0/article/likings',
  data: {
    target
  }
})

// 取消点赞请求
export const cancelLike = target => request({
  method: 'DELETE',
  url: `/v1_0/article/likings/${target}`
})

// 获取用户个人信息
export const getUserProfile = data => request({
  method: 'GET',
  url: '/v1_0/user/profile'
})

// 编辑用户个人信息
export const editUserProfile = data => request({
  method: 'PATCH',
  url: '/v1_0/user/profile',
  data
})

// 编辑用户头像
export const editUserPhoto = data => request({
  method: 'PATCH',
  url: '/v1_0/user/photo',
  data
})
