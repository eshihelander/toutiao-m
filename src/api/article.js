/**
 * 获取文章详情模块
 */

import request from '@/utils/request'

// 获取文章详情
export const getArticleById = articleId => request({
  url: `/v1_0/articles/${articleId}`
})
