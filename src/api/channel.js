import request from '@/utils/request'

// 获取所有频道列表

export const getAllChannels = () => request({
  method: 'GET',
  url: '/v1_0/channels'
})

// 设置用户频道 body c参数 channel 是一个包含了对象的数组
export const setUserChannel = channel => request({
  method: 'PATCH',
  url: '/v1_0/user/channels',
  data: {
    channels: [channel]
  }
})

// 删除指定用户频道
export const delUserChannel = target => request({
  method: 'DELETE',
  url: `/v1_0/user/channels/${target}`
})
