/**
 * 用户搜索模块
 */

import request from '@/utils/request'

// 发起搜索联想建议的请求
export const getSearchSuggest = q => request({
  url: '/v1_0/suggestion',
  params: {
    q
  }
})

// 获取搜索结果
export const getSearchResult = params => request({
  url: '/v1_0/search',
  params
})
