/**
 * home首页请求相关模块
 */
import request from '@/utils/request'

/**
 *  获取用户频道列表
 *  不强制用户登录，匿名用户返回后台设置的默认频道列表
 */
export const getUserChannel = () => request({
  method: 'GET',
  url: '/v1_0/user/channels'
  // headers: {
  //   Authorization: 'Bearer ' + store.state.user.token
  // }
})

/**
 * 获取文章新闻推荐
 */

export const getArticlRecommend = params => request({
  url: '/v1_0/articles',
  params
  // headers: {
  //   Authorization: 'Bearer ' + store.state.user.token
  // }
})
