import request from '@/utils/request'

/**
 * 用户评论请求模块
 */

// 获取获取评论或评论回复列表信息
export const getCommentList = params => request({
  url: '/v1_0/comments',
  params
})

// 对评论或评论回复点赞
export const addLikeComment = target => request({
  method: 'POST',
  url: '/v1_0/comment/likings',
  data: {
    target
  }
})

// 取消评论点赞
export const cancelLikeComment = target => request({
  method: 'DELETE',
  url: `/v1_0/comment/likings/${target}`
})

// 对文章或者评论进行评论
export const publishComment = data => request({
  method: 'POST',
  url: '/v1_0/comments',
  data
})
