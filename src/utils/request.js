/**
 * 请求模块
 */
import axios from 'axios'
import store from '@/store'

// 方式一
// axios.defaults.baseURL = 'http://toutiao.itheima.net'
// export default axios

// 方式二
const request = axios.create({
  // baseURL: 'http://toutiao.itheima.net'
  baseURL: 'http://geek.itheima.net'
})

/**
 * 请求拦截器
 */

// Add a request interceptor
request.interceptors.request.use(function (config) {
  // Do something before request is sent
  // config ：本次请求的配置对象
  // config 里面有一个属性：headers
  // 这里不是所有的请求都要拦截下来添加 token 的吧？缺少判断
  const { user } = store.state
  if (user && user.token) { // 拦截所有请求  但是只对用户登录后的请求添加 token
    config.headers.Authorization = `Bearer ${user.token}`
  }
  // console.log(config)
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

export default request
