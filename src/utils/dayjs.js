import dayjs from 'dayjs'
import Vue from 'vue'

// 加载中文语言包
import 'dayjs/locale/zh-cn'

// 导入 RelativeTime 插件
import relativeTime from 'dayjs/plugin/relativeTime'

// dayjs 默认显示的是英文 这里配置为中文显示
dayjs.locale('zh-cn')

// 使用 RelativeTime 插件
dayjs.extend(relativeTime)

// 配置全局过滤器 这样就可以在项目的任何组件的模板中使用
// 过滤器就相当于一个全局可用的方法 (仅供模板使用)
// 参数1：过滤器名称
// 参数2：过滤器函数体
// 使用方式： {{ 表达式 | 过滤器名称 }}
// 管道符前面的表达式的结果会作为参数传递到过滤器函数中
// 过滤器函数体的返回值会渲染到使用过滤器的模板位置
Vue.filter('RelativeTime', value => {
  // console.log(value) // 接收管道符前面的表达式的结果
  return dayjs().to(value)
})
