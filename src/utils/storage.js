/**
 * 封装本地存储模块
 */

/**
 * 存储数据
 */

export const setItem = (key, value) => {
  // 将数组、对象类型的数据转换为 JSON 格式字符串进行存储
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(key, value)
}

/**
 * 获取数据
 */
export const getItem = (key) => {
  const data = window.localStorage.getItem(key)
  // try catch 如果data是一个JSON字符串 就尝试用JSON.parse来解析成对象
  // 如果解析失败就会报错 进入到catch中 就把data原地返回 用if判断是否是JSON字符串的方法也可以实现 但是麻烦
  try {
    return JSON.parse(data)
  } catch (err) {
    return data
  }
}

/**
 * 删除数据
 */
export const removeItem = (key) => {
  window.localStorage.removeItem(key)
}
